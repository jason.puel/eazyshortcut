# coding: utf-8
#  ******************************************************************************
#  * @file           : MainPage.py
#  * @brief          : Manage Ubuntu Shortcut 
#  ******************************************************************************
import __future__
import os
import sys
from src.shortcut import DesktopEntry
from PyQt5.QtWidgets import (QApplication, QMainWindow, QWidget, QVBoxLayout, QAction, QFileDialog, qApp,
	        QCheckBox, QComboBox, QLabel, QLineEdit, QTextEdit)
from PyQt5.QtGui import QIcon
from functools import partial
import logging

__version__ = '0.1'
__author__  = 'Jason PUEL'
__date__    = "Mai 22, 2024"

DEFAULT_DESKTOPENTRY_PATH = r'~/.local/share/applications/'

def setup_logging(loggingLevel=logging.INFO,
        savelog=False,
        logpath=os.path.dirname(__file__),
        logfile='Auto_Report.log') -> logging:
    """Start recording logs on consol_log and stream it on the terminal"""

    logformat = '%(asctime)s:%(msecs)03d %(levelname)s - %(funcName)s: %(message)s'
    dateformat = "%Y/%m/%d %H:%M:%S"
    
    if savelog:
        logfullpath = os.path.join(logpath, logfile)
        from sys import stdout
        # When you use a logger with save file option, you need an stdout handler to display it in prompt
        logging.basicConfig(
            format=logformat, datefmt=dateformat,
            level=loggingLevel, 
            handlers=[logging.FileHandler(logfullpath),
                      logging.StreamHandler(stdout)])
    else:
        logging.basicConfig(format=logformat, datefmt=dateformat, level=loggingLevel)
    logger = logging.getLogger(__name__)
    return logger

#---------------------------Create Windows -------------------------------------------------- 

class SettingWindow(QWidget):
	def __init__(self,title:str):
		super().__init__()
		self.title = title
		self.initUI()

	def initUI(self):
		layout = QVBoxLayout()

		self.titleText = QTextEdit()
		self.titleText.setText("Name of the Shortcut")
		layout.addWidget(self.titleText)

		self.setLayout(layout)
		self.setGeometry(350, 350, 400, 200)
		self.setWindowTitle(self.title)
		self.setWindowIcon(QIcon(os.path.join(os.path.dirname(__file__), 'src','shortcut_23045.ico')))
		self.show()
		logging.debug(f'Set Window : {self.title}')

class PreviewWindow(QWidget):
	def __init__(self,title:str,shortcut:DesktopEntry, data, previewinfo):
		super().__init__()
		self.title = title
		#self.shortcuttool = shortcut
		self.previewinfo = previewinfo if previewinfo != '' else 'Path not defined'
		self.data = data if data else ''
		self.initUI()

	def updateDataPreview(self):
		self.data = self._dataPreview.toPlainText()

	def initUI(self):
		layout = QVBoxLayout()

		msgBox = QLabel(self.previewinfo)
		layout.addWidget(msgBox)

		self._dataPreview = QTextEdit()
		self._dataPreview.setPlainText(self.data)
		layout.addWidget(self._dataPreview)
		self._dataPreview.setReadOnly(True)
		#self._dataPreview.textChanged.connect(self.updateDataPreview)

		self.setLayout(layout)
		self.setGeometry(300, 300, 300, 450)
		self.setWindowTitle(self.title)
		self.setWindowIcon(QIcon(os.path.join(os.path.dirname(__file__), 'src','shortcut_23045.ico')))
		self.show()
		logging.debug(f'Set Window : {self.title}')

class MainWindow(QMainWindow):
	def __init__(self,title:str):
		super().__init__()
		self.title = title
		self.shortcut = DesktopEntry()
		
		self.bindingAction()
		self.bindcontextMenu()
		self.initUI()
		self.newfile()

	def newfile(self):
		"""Restet/init UI and shortcut"""
		self.filePath=''
		self.shortcut.init()

		self._titleBox.setText('New_DesktopEntry')
		self._cmdBox.setText('')
		self._CategBox.setCurrentIndex(0)
		self._IconBox.setText('~/.local/share/icons/custom/Blocknote.png')

	def savefile(self):
		"""Save data in self.filePath"""
		if self.shortcut.dataShortCut == {}:
			self.shortcut.init()
		try:
			if os.path.isfile(self.filePath) == True :
				self.shortcut.save(self.filePath)
			elif self.filePath == '':
				self.saveasfile()
			else : 
				self.shortcut.save(self.filePath)

		except AttributeError :
			logging.error(AttributeError)

	def saveasfile(self):
		self.filePath = QFileDialog.getSaveFileName(self, 'Save File', DEFAULT_DESKTOPENTRY_PATH, "Desktop files (*.desktop)")[0]
		self.shortcut.save(self.filePath)

	def createPreviewWin(self):
		"""Open a PreviewWindow: QWidjet Window"""
		DesktopEntity = self.shortcut.convertDicoIntoStr(self.shortcut.dataShortCut)
		self.previewWin = PreviewWindow(title="Preview", shortcut = self.shortcut, data=DesktopEntity,previewinfo=self.filePath, )
		self.previewWin.show()
		# TODO link data from preview to main Win
		
	def createSettingWin(self):
		"""Open a SettingWindow: QWidjet Window"""
		self.settingWin = SettingWindow(title="Setting")
		self.settingWin.show()

	def getfile(self):
		"""Load data from file"""
		self.filePath = QFileDialog.getOpenFileName(self, 'Open file', DEFAULT_DESKTOPENTRY_PATH, "Desktop files (*.desktop)")[0]
		logging.debug(self.filePath)
		self.shortcut.load(self.filePath)

		self._titleBox.setText(self.shortcut.dataShortCut["Name"])
		self._cmdBox.setText(self.shortcut.dataShortCut["Exec"])
		self._CategBox.setCurrentText(self.shortcut.dataShortCut["Categories"])
		self._IconBox.setText(self.shortcut.dataShortCut["Icon"])

	def bindingAction(self):
		"""Connecte QActions to fonctions"""
		logging.debug('')

		self._newAct = QAction('New', self)
		self._newAct.setStatusTip('Creat New Shortcut')
		self._newAct.triggered.connect(self.newfile)
		self._newAct.setShortcut('Ctrl+N')
		
		self._saveAct = QAction('Save', self)
		self._saveAct.setStatusTip('Save file')
		self._saveAct.triggered.connect(self.savefile)
		self._saveAct.setShortcut('Ctrl+S')
		
		self._saveAsAct = QAction('Save As ', self)
		self._saveAsAct.setStatusTip('Save file as')
		self._saveAsAct.triggered.connect(self.saveasfile)
		self._saveAsAct.setShortcut('Ctrl+Shift+S')
		
		self._openAct = QAction('Open', self)
		self._openAct.setStatusTip('Open file')
		self._openAct.triggered.connect(self.getfile)
		self._openAct.setShortcut('Ctrl+I')
		
		self._exitAct = QAction(QIcon('exit24.png'), '&Exit', self)
		self._exitAct.setStatusTip('Exit application')
		self._exitAct.triggered.connect(qApp.quit)
		self._exitAct.setShortcut('Ctrl+Q')
		
		self._prevAct = QAction('Preview',self)
		self._prevAct.triggered.connect(self.createPreviewWin)

		# I find no need for this Setting Windows
		#self._settAct = QAction('Setting', self)
		#self._settAct.triggered.connect(self.createSettingWin)
		#self._settAct.setShortcut('Ctrl+,')
		
	def bindcontextMenu(self):
		"""Creat menu bar : link buttum and QAction"""
		logging.debug('')
		self.statusBar()
		self.menubar = self.menuBar()
		
		fileMenu = self.menubar.addMenu('&File')
		fileMenu.addAction(self._newAct)
		fileMenu.addAction(self._saveAct)
		fileMenu.addAction(self._saveAsAct)
		fileMenu.addAction(self._openAct)
		fileMenu.addSeparator()
		#fileMenu.addAction(self._settAct)
		fileMenu.addAction(self._exitAct)

		fileMenu = self.menubar.addMenu('&Edit')
		fileMenu.addAction(self._prevAct)
	
	def updateValue(self,key:str, value):
		logging.debug(f'{key} => {value}')
		if type(value) is not str:
			self.shortcut.dataShortCut[key] = bool(value)
		else:
			self.shortcut.dataShortCut[key] = value

	def initUI(self):
		layout = QVBoxLayout()

		msgBox = QLabel('Name of the Shortcut')
		layout.addWidget(msgBox)
		self._titleBox = QLineEdit()
		self._titleBox.textChanged.connect(partial(self.updateValue,"Name"))
		layout.addWidget(self._titleBox)

		msgBox = QLabel("Command to execut")
		layout.addWidget(msgBox)
		self._cmdBox = QLineEdit()
		self._cmdBox.textChanged.connect(partial(self.updateValue,"Exec"))
		layout.addWidget(self._cmdBox)

		msgBox = QLabel("Categories")
		layout.addWidget(msgBox)
		self._CategBox = QComboBox()
		self._CategBox.addItems(['None', 'Application', 'Game', 'System', 'Settings'])
		self._CategBox.currentTextChanged.connect(partial(self.updateValue,"Categories"))
		layout.addWidget(self._CategBox)

		msgBox = QLabel("Icon")
		layout.addWidget(msgBox)
		self._IconBox = QLineEdit()
		self._IconBox.textChanged.connect(partial(self.updateValue,"Icon"))
		layout.addWidget(self._IconBox)

		self._StartupNotify = QCheckBox()
		self._StartupNotify.setText("Startup Notify")
		self._StartupNotify.stateChanged.connect(partial(self.updateValue, "StartupNotify"))
		layout.addWidget(self._StartupNotify)
			
		self._NoDisplay = QCheckBox()
		self._NoDisplay.setText("No Display")
		self._NoDisplay.stateChanged.connect(partial(self.updateValue, "NoDisplay"))
		layout.addWidget(self._NoDisplay)

		self._Terminal = QCheckBox()
		self._Terminal.setText("Show Terminal")
		self._Terminal.stateChanged.connect(partial(self.updateValue,"Terminal"))
		layout.addWidget(self._Terminal)

		widgets = QWidget()
		widgets.setLayout(layout)
		self.setCentralWidget(widgets)

		self.setGeometry(300, 300, 300, 250)
		self.setWindowTitle(self.title)
		self.setWindowIcon(QIcon(os.path.join(os.path.dirname(__file__), 'src','shortcut_23045.ico')))
		self.show()
		logging.debug(f'Set Window : {self.title}')

#--------------------------- Main Program ---------------------------------------------------

def main():
	# Création d'une application Qt
	app = QApplication.instance() 
	if not app:
		app = QApplication(sys.argv)
	# Creation de la fenetre principale
	MainWindow(title="Easy Shortcut")
	sys.exit(app.exec_())

if __name__ == "__main__":
	# Setup logger
	#logger = setup_logging(loggingLevel = logging.DEBUG)

	main()
