Eazy Shortcut
====
<a href="https://www.python.org" target="_blank" rel="noreferrer"> 
<img src="https://img.shields.io/badge/Python-14354C?style=for-the-badge&logo=python&logoColor=white" alt="python"/>
</a>

![Completion](https://img.shields.io/badge/Completion-90%25-brightgreen.svg)

## Description
It is juste an attempte to make unix desktop file with graphique interface.

![CaptureMainWin](./img/CaptureMainWin.png)

## Compiling Python

need pyinstaller : ```python -m pip install pyinstaller```

run :```pyinstaller.exe -D -F --noconsole MainPage.py ```
