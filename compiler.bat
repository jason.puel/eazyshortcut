@echo off
SET name=Eazy_ShortCut

:compiling
REM Compiling
ECHO ---Compiling---
CALL pyinstaller.exe --onedir --onefile --noconsole --name %name% --icon=src/shortcut_23045.ico MainPage.py && GOTO:cleaning || GOTO:pause

:cleaning
ECHO ---Cleaning---
RD /s /q build && echo rm build
DEL %name%.spec && echo rm .spec
GOTO:pause 

:pause
PAUSE
