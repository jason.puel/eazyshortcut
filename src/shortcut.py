# coding: utf-8
#  ******************************************************************************
#  * @file           : shortcut.py
#  * @brief          : Manage Ubuntu Shortcut 
#  ******************************************************************************
import __future__
import os
#import sys
#from configparser import ConfigParser
import logging

__version__ = '0.2'
__author__  = 'Jason PUEL'
__date__    = "September 11, 2022"

#---------------------------Create Shortcut-------------------------------------------------- 
class DesktopEntry():
    def __init__(self):
        super().__init__()
        self.dataShortCut : dict = {}

    def init(self):
        """Creat an dico as an empty desktop entity"""
        if self.dataShortCut != {}:
              logging.debug('Dico was not empty. earsed it')

        self.dataShortCut = {
            "Version" : "0.0",
            "Terminal" : False,
            "Type" : None,
            "Name" : None,
            "GenericName" : None,
            "Exec" : None,
            "Icon" : '~/.local/share/icons/custom/Blocknote.png',
            "StartupNotify" : False,
            "Categories" : None,
            "NoDisplay" : False
        }
        logging.debug(f'New shortcut created')
	
    def delet(self, KeyToRemove:str):
        """Remove a certain key in self.dataShortCut dico"""
        try:
            self.dataShortCut.pop(KeyToRemove)
            logging.debug(f'Removed key :{KeyToRemove}')
        except:
            if self.dataShortCut.get(KeyToRemove):
                logging.error('Key was not removed')
            else: logging.warn("Key don't existe")

    def add(self, KeyToAdd:str, Value=None):
        """Add a new key/value in self.dataShortCut dico"""
        if self.dataShortCut.get(KeyToAdd):
                logging.debug(f"Key already exist; key: '{KeyToAdd}' to value:'{Value}'")
        else: 
            self.dataShortCut.update({KeyToAdd : Value})
            self.dataShortCut[KeyToAdd] = Value
            logging.debug(f"Add key :'{KeyToAdd}' to value:'{Value}'")

    @staticmethod
    def convertDicoIntoStr(dictionary:dict):
        DesktopEntry = ''
        for element in dictionary :
            DesktopEntry = DesktopEntry + element+' = '+str(dictionary.get(element))+'\n'
        return DesktopEntry

    @staticmethod
    def convertStrIntoDico(strData:str):
        builsDictionary = {}
        if type(strData) == list():
            strData = strData.split('\n')
        for data in strData:
            if data == '':
                pass
            key = data.split('=')[0]
            value = data.split('=')[1]

            # Remove unnesessary withspace
            if key[-1:] == ' ':
                key = key[:-1]
            if value[0] == ' ':
                value = value[1:]
            # Convert str False|True into boolean
            value_map = {'TRUE': True, 'FALSE': False}
            value = value_map.get(value.upper(), value)

            # Add to dico
            builsDictionary.update({key : value})
            builsDictionary[key] = value
        
        return builsDictionary

    def show(self):
        """Show dico in prompt using print fonction"""
        dicotoshow = self.convertDicoIntoStr(self.dataShortCut)

        print('-------------------------------------\n')
        print(dicotoshow)
        print('-------------------------------------\n')

    def save(self, filename):
        version = self.dataShortCut["Version"].split(".")
        version[1] = str(int(version[1]) + 1)
        self.dataShortCut["Version"] = f'{version[0]}.{version[1]}'

        DesktopEntry = "#!/usr/bin/env xdg-open \n[Desktop Entry]\n"
        DesktopEntry += self.convertDicoIntoStr(self.dataShortCut)
        try:
            with open(filename, 'w') as file:
                file.write(DesktopEntry)
            logging.debug(f'File Saved : {filename}')
        except:
            logging.error('File not saved')

    def load(self, fileToLoad:str):
        if os.path.isfile(fileToLoad):
            self.filename = fileToLoad
            try:
                with open(self.filename, 'r') as file:
                    datafromfileLoad = file.read()
            except:
                logging.error('File not Load')
            self.dataShortCut = self.convertStrIntoDico(datafromfileLoad.splitlines()[2:])
        else:
            logging.warning(f'File unreachable : {fileToLoad}')

#--------------------------- Main Program ---------------------------------------------------

if __name__ == "__main__":
    """
    Code Exemple :

    toto = Shortcut()
    #toto.load(os.path.join(os.path.dirname(__file__),'Shortcut.desktop.temp'))
    toto.init()
    toto.show()
    toto.add('exemple','value exemple')
    toto.show()
    toto.delet('exemple')
    toto.show()
    #toto.save(os.path.join(os.path.dirname(__file__),'Shortcut1.desktop.temp'))
    """
    pass
